package cs3500.hw03;

import static org.junit.Assert.*;

import cs3500.hw02.FreecellModel;
import cs3500.hw02.Card;
import cs3500.hw02.FreecellOperations;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import org.junit.Before;
import org.junit.Test;

public class FreecellControllerTest {


  OutputStream out = new ByteArrayOutputStream();
  StringReader in;
  FreecellOperations<Card> model = new FreecellModel();

  @Before
  public void Initialize() {
    out = new ByteArrayOutputStream();
    model = new FreecellModel();
  }

  @Test
  public void InvalidNumCascades() {
    FreecellController controller = new FreecellController(new StringReader("sdsds"),
        new PrintStream(out));
    controller.playGame(this.model.getDeck(), this.model, 3, 4, false);
    assertEquals(out.toString(), "Could not start Game.");

  }

  @Test
  public void InvalidNumOpens() {
    FreecellController controller = new FreecellController(new StringReader("sdsd"),
        new PrintStream(out));
    controller.playGame(this.model.getDeck(), this.model, 5, 0, true);
    assertEquals(out.toString(), "Could not start Game.");
  }

  @Test(expected = IllegalArgumentException.class)
  public void nullDeck() {
    FreecellController controller= new FreecellController(new StringReader("sdsds"),
        new PrintStream(out));
    controller.playGame(null, this.model, 5, 4, true);

  }

  @Test(expected = IllegalArgumentException.class)
  public void nullModel() {
    FreecellController controller = new FreecellController(new StringReader("sdsds"),
        new PrintStream(out));
    controller.playGame(this.model.getDeck(), null, 5, 4, true);
  }

  @Test
  public void emptyString() {
    FreecellController controller = new FreecellController(new StringReader(""),
        new PrintStream(out));
    controller.playGame(this.model.getDeck(), this.model, 5, 4, false);

    assertEquals("F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♣, J♦, 10♠, 8♥, 7♣, 6♦, 5♠, 3♥, 2♣, A♦\n"
        + "C2: K♣, Q♦, J♠, 9♥, 8♣, 7♦, 6♠, 4♥, 3♣, 2♦, A♠\n"
        + "C3: K♦, Q♠, 10♥, 9♣, 8♦, 7♠, 5♥, 4♣, 3♦, 2♠\n"
        + "C4: K♠, J♥, 10♣, 9♦, 8♠, 6♥, 5♣, 4♦, 3♠, A♥\n"
        + "C5: Q♥, J♣, 10♦, 9♠, 7♥, 6♣, 5♦, 4♠, 2♥, A♣", out.toString());
  }

  @Test
  public void outOfBoundsDestination() {
    FreecellController controller = new FreecellController(new StringReader("C1 12 O5"),
        new PrintStream(out));
    controller.playGame(this.model.getDeck(), this.model, 5, 4, false);
    assertEquals("F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♣, J♦, 10♠, 8♥, 7♣, 6♦, 5♠, 3♥, 2♣, A♦\n"
        + "C2: K♣, Q♦, J♠, 9♥, 8♣, 7♦, 6♠, 4♥, 3♣, 2♦, A♠\n"
        + "C3: K♦, Q♠, 10♥, 9♣, 8♦, 7♠, 5♥, 4♣, 3♦, 2♠\n"
        + "C4: K♠, J♥, 10♣, 9♦, 8♠, 6♥, 5♣, 4♦, 3♠, A♥\n"
        + "C5: Q♥, J♣, 10♦, 9♠, 7♥, 6♣, 5♦, 4♠, 2♥, A♣"
        + "\nInvalid move. Try again.", out.toString());
  }

  @Test
  public void moveToCascade() {
    FreecellController controller = new FreecellController(new StringReader("C1 11 O4"),
        new PrintStream(out));
    controller.playGame(this.model.getDeck(), this.model, 5, 4, false);
    assertEquals("F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♣, J♦, 10♠, 8♥, 7♣, 6♦, 5♠, 3♥, 2♣, A♦\n"
        + "C2: K♣, Q♦, J♠, 9♥, 8♣, 7♦, 6♠, 4♥, 3♣, 2♦, A♠\n"
        + "C3: K♦, Q♠, 10♥, 9♣, 8♦, 7♠, 5♥, 4♣, 3♦, 2♠\n"
        + "C4: K♠, J♥, 10♣, 9♦, 8♠, 6♥, 5♣, 4♦, 3♠, A♥\n"
        + "C5: Q♥, J♣, 10♦, 9♠, 7♥, 6♣, 5♦, 4♠, 2♥, A♣\n"
        + "F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4: A♦\n"
        + "C1: K♥, Q♣, J♦, 10♠, 8♥, 7♣, 6♦, 5♠, 3♥, 2♣\n"
        + "C2: K♣, Q♦, J♠, 9♥, 8♣, 7♦, 6♠, 4♥, 3♣, 2♦, A♠\n"
        + "C3: K♦, Q♠, 10♥, 9♣, 8♦, 7♠, 5♥, 4♣, 3♦, 2♠\n"
        + "C4: K♠, J♥, 10♣, 9♦, 8♠, 6♥, 5♣, 4♦, 3♠, A♥\n"
        + "C5: Q♥, J♣, 10♦, 9♠, 7♥, 6♣, 5♦, 4♠, 2♥, A♣", out.toString());
  }

  @Test
  public void quitCardIndex() {
    FreecellController controller = new FreecellController(new StringReader("C1 Q 04"),
        new PrintStream(out));
    controller.playGame(this.model.getDeck(), this.model, 5, 4, false);
    assertEquals("F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♣, J♦, 10♠, 8♥, 7♣, 6♦, 5♠, 3♥, 2♣, A♦\n"
        + "C2: K♣, Q♦, J♠, 9♥, 8♣, 7♦, 6♠, 4♥, 3♣, 2♦, A♠\n"
        + "C3: K♦, Q♠, 10♥, 9♣, 8♦, 7♠, 5♥, 4♣, 3♦, 2♠\n"
        + "C4: K♠, J♥, 10♣, 9♦, 8♠, 6♥, 5♣, 4♦, 3♠, A♥\n"
        + "C5: Q♥, J♣, 10♦, 9♠, 7♥, 6♣, 5♦, 4♠, 2♥, A♣\n"
        + "Game quit prematurely." , out.toString());
  }


  @Test
  public void quitSourcePile() {
    FreecellController controller = new FreecellController(new StringReader("Q 11 04"),
        new PrintStream(out));
    controller.playGame(this.model.getDeck(), this.model, 5, 4, false);
    assertEquals("F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♣, J♦, 10♠, 8♥, 7♣, 6♦, 5♠, 3♥, 2♣, A♦\n"
        + "C2: K♣, Q♦, J♠, 9♥, 8♣, 7♦, 6♠, 4♥, 3♣, 2♦, A♠\n"
        + "C3: K♦, Q♠, 10♥, 9♣, 8♦, 7♠, 5♥, 4♣, 3♦, 2♠\n"
        + "C4: K♠, J♥, 10♣, 9♦, 8♠, 6♥, 5♣, 4♦, 3♠, A♥\n"
        + "C5: Q♥, J♣, 10♦, 9♠, 7♥, 6♣, 5♦, 4♠, 2♥, A♣\n"
        + "Game quit prematurely." , out.toString());
  }

  @Test
  public void quitDestinationPile() {
    FreecellController controller = new FreecellController(new StringReader("C1 12 q"),
        new PrintStream(out));
    controller.playGame(this.model.getDeck(), this.model, 5, 4, false);
    assertEquals("F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♣, J♦, 10♠, 8♥, 7♣, 6♦, 5♠, 3♥, 2♣, A♦\n"
        + "C2: K♣, Q♦, J♠, 9♥, 8♣, 7♦, 6♠, 4♥, 3♣, 2♦, A♠\n"
        + "C3: K♦, Q♠, 10♥, 9♣, 8♦, 7♠, 5♥, 4♣, 3♦, 2♠\n"
        + "C4: K♠, J♥, 10♣, 9♦, 8♠, 6♥, 5♣, 4♦, 3♠, A♥\n"
        + "C5: Q♥, J♣, 10♦, 9♠, 7♥, 6♣, 5♦, 4♠, 2♥, A♣\n"
        + "Game quit prematurely." , out.toString());
  }

  @Test(expected = IllegalStateException.class)
  public void emptyInput() {
    FreecellController controller = new FreecellController(new StringReader(""),
        new PrintStream(out));
    controller.playGame(this.model.getDeck(), this.model, 5, 4, true);
  }

  @Test
  public void testFullGame() {

    String input = "C1 13 F1 C1 12 F1 C1 11 F1 C1 10 F1 C1 9 F1 C1 8 F1 C1 7 F1 C1 6 F1 C1 5 F1 "
        + "C1 4 F1 C1 3 F1 C1 2 F1 C1 1 F1 C2 13 F2 C2 12 F2 C2 11 F2 C2 10 F2 C2 9 F2 C2 8 "
        + "F2 C2 7 F2 C2 6 F2 C2 5 F2 C2 4 F2 C2 3 F2 C2 2 F2 C2 1 F2 C3 13 F3 C3 12 F3 C3 "
        + "11 F3 C3 10 F3 C3 9 F3 C3 8 F3 C3 7 F3 C3 6 F3 C3 5 F3 C3 4 F3 C3 3 F3 C3 2 F3 C3 "
        + "1 F3 C4 13 F4 C4 12 F4 C4 11 F4 C4 10 F4 C4 9 F4 C4 8 F4 C4 7 F4 C4 6 F4 C4 5 F4 "
        + "C4 4 F4 C4 3 F4 C4 2 F4 C4 1 F4";

    String output = "F1: A♥, 2♥, 3♥, 4♥, 5♥, 6♥, 7♥, 8♥, 9♥, 10♥, J♥, Q♥, K♥\n"
        + "F2: A♣, 2♣, 3♣, 4♣, 5♣, 6♣, 7♣, 8♣, 9♣, 10♣, J♣, Q♣, K♣\n"
        + "F3: A♦, 2♦, 3♦, 4♦, 5♦, 6♦, 7♦, 8♦, 9♦, 10♦, J♦, Q♦, K♦\n"
        + "F4: A♠, 2♠, 3♠, 4♠, 5♠, 6♠, 7♠, 8♠, 9♠, 10♠, J♠, Q♠, K♠\n"
        + "O1:\n" + "O2:\n" + "O3:\n" + "O4:\n" + "C1:\n" + "C2:\n" + "C3:\n" + "C4:";

    String gameover = "Game over";
    FreecellController controller = new FreecellController(new StringReader(input), new PrintStream(out));
    controller.playGame(this.model.getDeck(), model, 4, 4, false);
    assertEquals(gameover, out.toString().substring(13520));
  }







}