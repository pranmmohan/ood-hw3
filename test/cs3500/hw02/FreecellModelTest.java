package cs3500.hw02;

import static org.junit.Assert.assertEquals;
import java.util.List;
import java.util.Stack;

import org.junit.Test;

public class FreecellModelTest {

  FreecellModel emptyFreecellModel = new FreecellModel();
  FreecellModel toCascade = new FreecellModel();
  FreecellModel toFoundation = new FreecellModel();
  FreecellModel toOpenShuffle = new FreecellModel();
  FreecellModel noShuffle = new FreecellModel();
  FreecellModel emptyOpen = new FreecellModel();
  FreecellModel shuffled = new FreecellModel();


  /**
   * Checks whether shuffling a deck produces a deck with 52
   * cards in random positions.
   */
  @Test
  public void shuffleDeck() {
    emptyFreecellModel.startGame(emptyFreecellModel.getDeck(),
        8, 4, true);
    assertEquals(52, this.emptyFreecellModel.getDeck().size());
  }


  /**
   * Checks whether a valid move can be made from an open
   * pile to a cascade pile.
   */
  @Test
  public void moveToCascade() {
    toCascade.startGame(toCascade.getDeck(), 8, 5,
        true);
    toCascade.getOpenPiles()[1].add(new Card(Values.Jack, Suits.Diamond));
    toCascade.getCascadePiles()[2].add(new Card(Values.Queen, Suits.Spade));

    toCascade.move(PileType.OPEN, 1, 0, PileType.CASCADE,
        2);
    assertEquals(9, toCascade.getCascadePiles()[2].size());

    assertEquals(Suits.Diamond,
        toCascade.getCascadePiles()[2].get(8).getSuit());
    assertEquals(Values.Jack,
        toCascade.getCascadePiles()[2].get(8).getValue());
  }


  /**
   * Valid move from a cascade pile to a foundation pile.
   */
  @Test
  public void moveToFoundation() {
    toFoundation.startGame(toFoundation.getDeck(), 8,
        3, false);
    toFoundation.getCascadePiles()[1].add(new Card(Values.Ace, Suits.Spade));

    toFoundation.move(PileType.CASCADE, 1, 7,
        PileType.FOUNDATION, 0);
    assertEquals(1,
        toFoundation.getFoundationPiles()[0].size());

    
  }




  /**
   * Valid move from an open pile to another empty
   * open pile with a nonshuffled deck.
   */
  @Test
  public void moveToEmpyOpenNoShuffle() {
    emptyFreecellModel.startGame(emptyFreecellModel.getDeck(),
        8, 4, false);
    assertEquals(4, this.emptyFreecellModel.getOpenPiles().length);
    emptyFreecellModel.getOpenPiles()[0].add(new Card(Values.Ace, Suits.Spade));
    emptyFreecellModel.move(PileType.OPEN, 0, 0, PileType.OPEN,
        1);
    assertEquals(0, this.emptyFreecellModel.getOpenPiles()[0].size());
    assertEquals(1, this.emptyFreecellModel.getOpenPiles()[1].size());
  }


  /**
   * Valid move from an open pile to another empty
   * open pile with a valid shuffled deck.
   */
  @Test 
  public void moveToOpenShuffle() {
    toOpenShuffle.startGame(toOpenShuffle.getDeck(),
        8, 4, true);
    toOpenShuffle.getCascadePiles()[0].add(new Card(Values.Ace, Suits.Spade));
    toOpenShuffle.move(PileType.CASCADE, 0, 7, PileType.OPEN,
        1);
    assertEquals(7, this.toOpenShuffle.getCascadePiles()[0].size());
    assertEquals(1, this.toOpenShuffle.getOpenPiles()[1].size());
    
  }

  /**
   * Checks if there are any duplicate cards
   * without shuffling the deck.
   */
  @Test
  public void noDuplicateCardsNoShuffle() {
    noShuffle.startGame(emptyFreecellModel.getDeck(),
        8, 4, false);

    int numHeartJack = 0;

    for (int i = 0; i < noShuffle.getDeck().size(); i++) {
      if (noShuffle.getDeck().get(i).getValue() == Values.Ace
          && noShuffle.getDeck().get(i).getSuit() == Suits.Heart) {
        numHeartJack++;
      }
    }

    assertEquals(1, numHeartJack);
  }


  /**
   * Checks if there are duplicates in the deck after shuffling.
   */
  @Test
  public void noDuplicationShuffle() {
    noShuffle.startGame(emptyFreecellModel.getDeck(),
        8, 4, true);

    int numHeartJack = 0;

    for (int i = 0; i < noShuffle.getDeck().size(); i++) {
      if (noShuffle.getDeck().get(i).getValue() == Values.Ace
          && noShuffle.getDeck().get(i).getSuit() == Suits.Heart) {
        numHeartJack++;
      }
    }

    assertEquals(1, numHeartJack);

  }


  /**
   * Try to move from an Empty Pile.
   */
  @Test(expected = IllegalArgumentException.class)
  public void EmptyOpenPile() {
    emptyOpen.startGame(emptyOpen.getDeck(), 5,
        2, false);


    emptyOpen.move(PileType.OPEN, 0, 0,
            PileType.OPEN, 1);

  }

  /**
   * Checks if the non shuffled deck is complete.
   */
  @Test(expected = IllegalArgumentException.class)
  public void IncompleteDeckNoShuffle() {
    List<Card> deck = new Stack<Card>();
    noShuffle.startGame(deck, 5,
        2, false);

  }


  /**
   * Checks if the shuffled deck is complete.
   */
  @Test(expected = IllegalArgumentException.class)
  public void IncompleteDeckShuffle() {
    List<Card> deck = new Stack<Card>();
    shuffled.startGame(deck, 5,
        2, false);

  }


  /**
   * Cards are dealt properly amongst the cascade piles
   * after shuffling.
   */
  @Test
  public void goodDealShuffle() {
    shuffled = new FreecellModel();
    shuffled.startGame(shuffled.getDeck(), 6, 4, true);

    assertEquals(9, shuffled.getCascadePiles()[0].size());
    assertEquals(9, shuffled.getCascadePiles()[3].size());
    assertEquals(8, shuffled.getCascadePiles()[4].size());
    assertEquals(8, shuffled.getCascadePiles()[5].size());
  }

  /**
   * Cards are dealt properly without shuffling.
   */

  @Test
  public void goodDealNoShuffle() {
    noShuffle = new FreecellModel();
    noShuffle.startGame(noShuffle.getDeck(), 6, 4, true);

    assertEquals(9, noShuffle.getCascadePiles()[1].size());
    assertEquals(9, noShuffle.getCascadePiles()[2].size());
    assertEquals(8, noShuffle.getCascadePiles()[4].size());
    assertEquals(8, noShuffle.getCascadePiles()[5].size());
    
  }


  /**
   * Duplicate cards in the deck after shuffling.
   */

  @Test(expected = IllegalArgumentException.class)
  public void duplicatesInShuffle() {
    shuffled = new FreecellModel();
    List<Card> shuffledDeck = shuffled.getDeck();
    shuffledDeck.add(new Card(Values.Ace, Suits.Spade));

    shuffled.startGame(shuffledDeck, 6,
        8, true );
  }


  /**
   * Duplicate cards in the deck without shuffling.
   */
  @Test(expected = IllegalArgumentException.class)
  public void duplicatesInNoShuffle() {
    noShuffle = new FreecellModel();
    List<Card> noShuffledDeck = noShuffle.getDeck();
    noShuffledDeck.add(new Card(Values.Five, Suits.Diamond));


    noShuffle.startGame(noShuffledDeck, 6,
        8, false);


  }

  /**
   * Invalid number of cascade piles as an argument.
   */
  @Test(expected = IllegalArgumentException.class)
  public void invalidNumCascades() {
    shuffled = new FreecellModel();
    shuffled.startGame(shuffled.getDeck(), 3,
        1, true);
  }


  /**
   * Cards are dealt properly with no shuffle.
   */
  @Test
  public void testDealCorrectlyNoShuffle() {
    String gameState = "F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♦, 10♥, 9♦, 7♥, 6♦, 4♥, 3♦, A♥\n"
        + "C2: K♣, Q♠, 10♣, 9♠, 7♣, 6♠, 4♣, 3♠, A♣\n"
        + "C3: K♦, J♥, 10♦, 8♥, 7♦, 5♥, 4♦, 2♥, A♦\n"
        + "C4: K♠, J♣, 10♠, 8♣, 7♠, 5♣, 4♠, 2♣, A♠\n"
        + "C5: Q♥, J♦, 9♥, 8♦, 6♥, 5♦, 3♥, 2♦\n"
        + "C6: Q♣, J♠, 9♣, 8♠, 6♣, 5♠, 3♣, 2♠";

    noShuffle = new FreecellModel();
    noShuffle.startGame(noShuffle.getDeck(), 6, 4,
        false);
    assertEquals(gameState, noShuffle.getGameState());
  }


  /**
   * Nothing is displayed if the game has not started.
   */
  @Test
  public void gameStateNoStart() {
    noShuffle = new FreecellModel();
    assertEquals("", noShuffle.getGameState());

  }

  /**
   * Piles are displayed properly after a card is moved.
   */
  @Test
  public void gameStateAfterMove() {
    noShuffle = new FreecellModel();
    noShuffle.startGame(noShuffle.getDeck(), 6, 4,
        false);
    noShuffle.move(PileType.CASCADE, 2, 8, PileType.OPEN, 0);
    String gameState = "F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1: A♦\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♦, 10♥, 9♦, 7♥, 6♦, 4♥, 3♦, A♥\n"
        + "C2: K♣, Q♠, 10♣, 9♠, 7♣, 6♠, 4♣, 3♠, A♣\n"
        + "C3: K♦, J♥, 10♦, 8♥, 7♦, 5♥, 4♦, 2♥\n"
        + "C4: K♠, J♣, 10♠, 8♣, 7♠, 5♣, 4♠, 2♣, A♠\n"
        + "C5: Q♥, J♦, 9♥, 8♦, 6♥, 5♦, 3♥, 2♦\n"
        + "C6: Q♣, J♠, 9♣, 8♠, 6♣, 5♠, 3♣, 2♠";

    assertEquals(gameState, noShuffle.getGameState());
  }














}