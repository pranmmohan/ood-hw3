package cs3500.hw03;
import cs3500.hw02.FreecellOperations;
import java.util.List;

public interface IFreecellController<K> {


  void playGame(List<K> deck, FreecellOperations<K> model,
      int numCascades, int numOpens, boolean shuffle);
}
