package cs3500.hw03;
import cs3500.hw02.Card;
import cs3500.hw02.FreecellOperations;
import cs3500.hw02.PileType;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.HashMap;

public class FreecellController implements IFreecellController<Card> {



  private Readable rd;
  private Appendable ap;
  private FreecellOperations<Card> model;
  HashMap <String, PileType> charToPileType = new HashMap<>();




  private boolean validateInteger(String integer) {

    try {
      int index = Integer.parseInt(integer);
      return index > 0;
    }
    catch (NumberFormatException e) {

      return false;
    }

  }

  private void validatePile(String source, Scanner scan) {

    if(source.length() == 2 && validateInteger(source.substring(1))) {
      switch (source.substring(0, 1)) {
        case "C":
          break;
        case "O":
          break;
        case "F":
          break;
        case "Q":
          break;
        case "q":
          break;
        default:
          if (scan.hasNext()) {
            validatePile(scan.next(), scan);
          }
          break;
      }
    }

    else if (scan.hasNext()){
      validatePile(scan.next(), scan);
    }

    return;



  }

  void validatePosition(String source, Scanner scan) {
    if(source.toLowerCase().equals("q") || validateInteger(source)) {
      return;
    }

    else if(scan.hasNext()){
      validatePosition(scan.next(), scan);
    }

    return;


  }

  void append(String output) {
    try {
      this.ap.append(output);
    }
    catch(IOException e) {
      return;
    }
  }


  @Override
  public void playGame(List<Card> deck, FreecellOperations<Card> model,  int numCascades,
      int numOpens, boolean shuffle) {


    if (deck == null || model == null) {
      throw new IllegalArgumentException("You have entered either an empty deck or model");
    }



    try {
      model.startGame(deck, numCascades, numOpens, shuffle);
      this.append(model.getGameState());

    }
    catch(IllegalArgumentException e) {
      this.append("Could not start game.");
      return;
    }



    Scanner scan = new Scanner(rd).useDelimiter("\\s+");

    if (!scan.hasNext()) {
      throw new IllegalStateException("No input provided");
    }

    while (scan.hasNext()) {


      try {
        String source = scan.next();
        if (source.toLowerCase().equals("q")) {
          this.append("\nGame quit prematurely.");
          return;
        }
        String position = scan.next();
        if (position.toLowerCase().equals("q")) {
          this.append("\nGame quit prematurely.");
          return;
        }
        String destination = scan.next();
        if (destination.toLowerCase().equals("q")) {
          this.append("\nGame quit prematurely.");
          return;
        }


        validatePile(source, scan);
        validatePosition(position, scan);
        validatePile(destination, scan);


        if (source.toLowerCase().equals("q") | position.toLowerCase().equals("q")
            | destination.toLowerCase().equals("q")) {
          this.append("\nGame quit prematurely.");
          return;

        }


        else {
          String sourcePileType = source.substring(0, 1);
          Integer sourcePileNumber = Integer.parseInt(source.substring(1)) - 1;

          int cardIndex = Integer.parseInt(position) - 1;

          String destinationPileType = destination.substring(0, 1);
          Integer destinationPileNumber = Integer.parseInt(destination.substring(1)) - 1;

          try {
            model.move(charToPileType.get(sourcePileType), sourcePileNumber,
                cardIndex, charToPileType.get(destinationPileType), destinationPileNumber);
              this.append("\n"+model.getGameState());
          }

          catch (IllegalArgumentException e) {
            this.append("\nInvalid move. Try again.");

          }

          // one or more of the arguemnts was null
          catch (NullPointerException e){
            return;
          }


        }


      }
      // not enough arguments passed in
      catch(IllegalStateException e) {
        return;
      }

      if(model.isGameOver()) {
        this.append(("\nGame over"));
        return;
      }


    }





  }


  public FreecellController(Readable rd, Appendable ap) {


    this.rd = rd;
    this.ap = ap;

    this.charToPileType.put("C", PileType.CASCADE);
    this.charToPileType.put("O", PileType.OPEN);
    this.charToPileType.put("F", PileType.FOUNDATION);

  }


}
