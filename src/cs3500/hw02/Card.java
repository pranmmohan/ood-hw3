package cs3500.hw02;



public class Card extends Object {

  private Values value;
  private Suits suit;


  /**
   * Constructor for the card class. It can only have a value or suit
   * listed in the respective enums.
   *
   * @param value the value of the card
   * @param suit the suit of the card
   */

  Card(Values value, Suits suit) {
    this.value = value;
    this.suit = suit;


  }

  @Override
  public int hashCode() {
    return this.value.getValue() + 30 * this.suit.name().length();
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Card)) {
      throw new ClassCastException("Not a card");
    }


    return this.value.equals(((Card)o).value)
        && this.suit.equals(((Card)o).suit);


  }

  /**
   * Checks whether the given card is the opposite color and is
   * exactly one less in value than this card.
   *
   * @param moveCard card that will be moved ontop of this card
   * @return whether this is a valid move onto a cascade pile
   */

  protected boolean validCascadeMove(Card moveCard) {

    return this.value.getValue() - 1 == moveCard.value.getValue()
        && this.suit.oppositeColors(moveCard.suit);

  }

  /**
   * Checks whether the given card is the same suit and one value greater
   * than this card.
   *
   * @param moveCard card that will be moved ontop of this card in a foundation pile
   * @return whether this card can be moved onto this card
   */

  protected boolean validFoundationMove(Card moveCard) {
    return this.suit == moveCard.suit && this.value.getValue() + 1 == moveCard.value.getValue();
  }




  @Override
  public String toString() {

    return this.value.toString() + this.suit.toString();

  }


  /**
   * Getter function of the cards suit.
   *
   * @return the suit of the card
   */
  public Suits getSuit() {
    return this.suit;
  }

  /**
   * Getter function of the cards value.
   *
   * @return the value of the card
   */

  public Values getValue() {
    return this.value;
  }

  public Card clone() {
    return new Card(this.value, this.suit);
  }

}
