package cs3500.hw02;

import java.util.List;
import java.util.LinkedList;
import java.util.Random;
import java.util.Stack;
import java.util.HashSet;



public class FreecellModel implements FreecellOperations<Card> {



  private List<Card> deck;
  private Stack<Card>[] cascadePiles;
  private Stack<Card>[] foundationPiles = new Stack[4];
  private Stack<Card>[] openPiles;
  private boolean gameStart = false;
  private HashSet<Card> duplicates = new HashSet<Card>();


  /**
   * Default constructor.
   */
  public FreecellModel() {
    this.deck = this.getDeck();
    gameStart = false;

  }



  /**
   * Constructor.
   * @param deck Constructs a Freecell model with the given deck.
   */
  public FreecellModel(List<Card> deck) {
    this.deck = deck;
  }


  /**
   * Checks the Deck for duplicaes.
   * @param deck to be checked for no duplicates.
   * @return whether it is a valid deck with no duplicates
   */
  private boolean checkDuplicates(List<Card> deck) {

    if (deck.size() != 52) {
      return false;
    }

    this.duplicates.clear();

    for (int i = 0; i < deck.size(); i ++) {
      this.duplicates.add(deck.get(i));
    }

    return this.duplicates.size() == 52;
  }

  public void playGame(List<Card> deck, FreecellOperations<Card> model, int numCascades, int numOpens,
      boolean shuffle) {




  }


  /**
   * Initalizes the deck with all possible valid cards.
   * @return a deck with all cards possible
   */


  public List<Card> getDeck() {


    List<Card> deck = new LinkedList<Card>();
    for (Values value: Values.values()) {
      for (Suits suit: Suits.values()) {
        deck.add(new Card(value, suit));
      }
    }



    return deck;
  }

  /**
   * Getter function for cascade piles.
   * @return the cascade piles
   */
  protected List<Card>[] getCascadePiles() {
    return this.cascadePiles;
  }

  /**
   * Getter function for the foundation piles.
   * @return the foundation piles
   */
  protected List<Card>[] getFoundationPiles() {
    return this.foundationPiles;
  }

  /**
   * Getter function for the open piles.
   * @return the open piles
   */
  protected List<Card>[] getOpenPiles() {
    return this.openPiles;
  }


  /**
   * Copies each Card in the Deck.
   * @param deck to be copied.
   * @return a shallow copy of the deck
   */
  private List<Card> copyDeck(List<Card> deck) {
    List<Card> copyDeck = new Stack<Card>();

    for (int i = 0; i < deck.size(); i++) {
      copyDeck.add(deck.get(i).clone());
    }

    return copyDeck;

  }

  /**
   * Shuffles the given deck.
   *
   * @param deck to be shuffled.
   * @return a shuffled deck
   */

  private List<Card> shuffle(List<Card> deck) {
    List<Card> shuffledDeck = new Stack<Card>();
    Random random = new Random();
    for (int i = deck.size(); i > 0; i--) {
      shuffledDeck.add(deck.remove(random.nextInt(i)));
    }


    return shuffledDeck;

  }


  /**
   * Deal a new game of freecell with the given deck, with or without shuffling
   * it first. This method first verifies that the deck is valid. It deals the
   * deck among the cascade piles in roundrobin fashion. Thus if there are 4
   * cascade piles, the 1st pile will get cards 0, 4, 8, ..., the 2nd pile will
   * get cards 1, 5, 9, ..., the 3rd pile will get cards 2, 6, 10, ... and the
   * 4th pile will get cards 3, 7, 11, .... Depending on the number of cascade
   * piles, they may have a different number of cards
   * @param deck            the deck to be dealt
   * @param numCascadePiles number of cascade piles
   * @param numOpenPiles    number of open piles
   * @param shuffle         if true, shuffle the deck else deal the deck as-is
   */
  @Override
  public void startGame(List<Card> deck , int numCascadePiles, int numOpenPiles, boolean shuffle)
                throws IllegalArgumentException {

    if (deck.size() != 52) {
      throw new IllegalArgumentException("Not enough cards in Deck");
    }


    if (numCascadePiles < 4 || numOpenPiles < 1) {
      throw new IllegalArgumentException("Not enough piles");
    }

    List<Card> copiedDeck = this.copyDeck(deck);
    if (shuffle) {
      List<Card> shuffledDeck = this.copyDeck(deck);
      this.deck = this.shuffle(shuffledDeck);
      copiedDeck = this.shuffle(copiedDeck);

    }

    if (!checkDuplicates(copiedDeck)) {
      throw new IllegalArgumentException("Invalid Deck");
    }



    this.gameStart = true;


    this.cascadePiles = new Stack[numCascadePiles];
    this.openPiles = new Stack[numOpenPiles];



    /*
      initialize the correct number of piles for the Cascade, Open, and Foundation Piles
     */
    for (int i = 0; i < numCascadePiles; i++) {
      this.cascadePiles[i] = new Stack<Card>();
    }

    for (int i = 0; i < numOpenPiles; i++) {
      this.openPiles[i] = new Stack<Card>();

    }

    for (int i = 0; i < 4; i++) {
      this.foundationPiles[i] = new Stack<Card>();
    }


    /*
      Distribute the deck amongst the cascade piles round robin.
     */

    for (int i = 0; i < this.deck.size(); i++) {
      this.cascadePiles[i % numCascadePiles].add(copiedDeck.remove(0));
    }



  }


  /**
   * Return the present state of the game as a string. The string is formatted
   * as follows:
   * <pre>
   * F1:[b]f11,[b]f12,[b],...,[b]f1n1[n] (Card in foundation pile 1 in order)
   * F2:[b]f21,[b]f22,[b],...,[b]f2n2[n] (Card in foundation pile 2 in order)
   * ...
   * Fm:[b]fm1,[b]fm2,[b],...,[b]fmnm[n] (Card in foundation pile m in
   * order)
   * O1:[b]o11[n] (Card in open pile 1)
   * O2:[b]o21[n] (Card in open pile 2)
   * ...
   * Ok:[b]ok1[n] (Card in open pile k)
   * C1:[b]c11,[b]c12,[b]...,[b]c1p1[n] (Card in cascade pile 1 in order)
   * C2:[b]c21,[b]c22,[b]...,[b]c2p2[n] (Card in cascade pile 2 in order)
   * ...
   * Cs:[b]cs1,[b]cs2,[b]...,[b]csps (Card in cascade pile s in order)
   *
   * where [b] is a single blankspace, [n] is newline. Note that there is no
   * newline on the last line
   * </pre>
   *
   * @return the formatted string as above
   */
  @Override
  public String getGameState() {
    if (!gameStart) {
      return "";
    }

    String gameState = "";

    for (int i = 0; i < foundationPiles.length ; i++) {
      gameState += String.format("F%d:", i + 1) + getGameStatePile(foundationPiles[i]) + "\n";
    }


    for (int i = 0; i < openPiles.length; i++) {
      //gameState += String.format("O%d:", i + 1) + openPiles[i].toString() + "\n";
      gameState += String.format("O%d:", i + 1) + getGameStatePile(openPiles[i]) + "\n";

    }

    for (int i = 0; i < cascadePiles.length; i++) {
      //gameState += String.format("C%d:", i + 1) + cascadePiles[i].toString() + "\n";
      gameState += String.format("C%d:", i + 1) + getGameStatePile(cascadePiles[i]) + "\n";

    }

    String s =  gameState.substring(0, gameState.length() - 1 );

    return gameState.substring(0, gameState.length() - 1 );
  }


  private String getGameStatePile(List<Card> pile) {

    String allCards = "";
    for (Card c: pile) {
      allCards += " " + c.toString() + ",";
    }

    if (allCards.length() != 0) {
      allCards = allCards.substring(0, allCards.length() - 1);
    }

    return allCards;

  }

  /**
   * Move a card from the given source pile to the given destination pile, if
   * the move is valid.
   *
   * @param sourceType         the type of the source pile see @link{PileType}
   * @param sourcePileNumber     the pile number of the given type, starting at 0
   * @param cardIndex      the index of the card to be moved from the source
   *                       pile, starting at 0
   * @param destinationType    the type of the destination pile (see
   * @param destPileNumber the pile number of the given type, starting at 0
   * @throws IllegalArgumentException if the move is not possible {@link
   *                                  PileType})
   */
  @Override
  public void move(PileType sourceType, int sourcePileNumber, int cardIndex,
                   PileType destinationType, int destPileNumber) {


    if (sourcePileNumber < 0 | destPileNumber < 0) {
      throw new IllegalArgumentException("Entered a negative index");
    }
    Card cardToMove = this.copyOfCardAt(sourceType, sourcePileNumber, cardIndex);


    String destinationPile = destinationType.name();

    switch (destinationPile) {

      case "OPEN":
        if (openPiles.length > destPileNumber
            && openPiles[destPileNumber].size() == 0) {
          Card removedCard = this.removeCard(sourceType, sourcePileNumber, cardIndex);
          openPiles[destPileNumber].add(removedCard);
        }
        else {
          throw new IllegalArgumentException("Cannot move this card onto this destination pile");
        }
        break;
      case "FOUNDATION":
        if (foundationPiles.length > destPileNumber) {
          if (foundationPiles[destPileNumber].size() == 0
              && cardToMove.getValue() == Values.Ace) {
            Card removedCard = this.removeCard(sourceType, sourcePileNumber, cardIndex);
            foundationPiles[destPileNumber].add(removedCard);

          }
          else if (foundationPiles[destPileNumber].peek()
              .validFoundationMove(cardToMove)) {
            Card removedCard = this.removeCard(sourceType, sourcePileNumber, cardIndex);
            foundationPiles[destPileNumber].add(removedCard);
          }
        }
        else {
          throw new IllegalArgumentException("cannot move this card onto this foundation pile");
        }
        break;
      case "CASCADE":
        if (cascadePiles.length > destPileNumber
            && (cascadePiles[destPileNumber].size() == 0
            || cascadePiles[destPileNumber].peek().validCascadeMove(cardToMove))) {
          Card removedCard = this.removeCard(sourceType, sourcePileNumber, cardIndex);
          cascadePiles[destPileNumber].add(removedCard);
        }
        else {
          throw new IllegalArgumentException("cannot move this card onto this cascade pile");
        }
        break;
      default:
        throw new IllegalArgumentException("entered invalid destination type");
    }

  }


  /**
   * Removes the top most card from the given pile.
   *
   * @param sourceType the type of pile the card will be moved from
   * @param sourcePileNumber which number pile
   * @param cardIndex which card from the pile
   * @return the removed card
   */
  private Card removeCard(PileType sourceType, int sourcePileNumber, int cardIndex) {

    String pileType = sourceType.name();

    switch (pileType) {

      case "OPEN":
        return openPiles[sourcePileNumber].remove(cardIndex);
      case "CASCADE":
        return cascadePiles[sourcePileNumber].remove(cardIndex);
      case "FOUNDATION":
        return foundationPiles[sourcePileNumber].remove(cardIndex);
      default:
        throw new IllegalArgumentException("Invalid piletype");


    }
  }

  /**
   * Gets a shallow copy of the card so that it can be used
   * to check if the move is valid before the card is actually
   * removed.
   *
   * @param sourceType the type of pile the card will be copied from
   * @param sourcePileNumber which number pile to choose
   * @param cardIndex which card from the pile to choose
   * @return a shallow copy of the card
   */

  private Card copyOfCardAt(PileType sourceType, int sourcePileNumber, int cardIndex) {


    String pileType = sourceType.name();
    switch (pileType) {

      case "OPEN":
        if (openPiles.length > sourcePileNumber
            && openPiles[sourcePileNumber].size() == 1 && cardIndex == 0) {
          Card retrieved =  openPiles[sourcePileNumber].get(0).clone();
          return retrieved;
        }
        else {
          throw new IllegalArgumentException("Cant remove that card");
        }


      case "CASCADE":
        if (cascadePiles.length > sourcePileNumber
            && cascadePiles[sourcePileNumber].size() - 1 == cardIndex) {
          Card retrieved = (Card) cascadePiles[sourcePileNumber].get(cardIndex).clone();
          return retrieved;
        }
        else {


          throw new IllegalArgumentException("Cant remove that card");
        }



      case "FOUNDATION":
        if (foundationPiles.length > sourcePileNumber
            && foundationPiles[sourcePileNumber].size() - 1 == cardIndex) {
          Card retrieved = foundationPiles[sourcePileNumber].get(cardIndex).clone();
          return retrieved;
        }
        else {
          throw new IllegalArgumentException("Cant remove that card");

        }

      default:
        throw new IllegalArgumentException("Entered an invalid type of PileType");

    }
  }


  /**
   * Signal if the game is over or not.
   * @return true if game is over.False if not
   */
  @Override
  public boolean isGameOver() {
    int totalNumCards = 0;
    for (int i = 0 ; i < getFoundationPiles().length; i++) {
      totalNumCards += this.getFoundationPiles()[i].size();
    }

    return totalNumCards == 52;
  }






}
