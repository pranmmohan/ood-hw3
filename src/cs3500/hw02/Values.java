package cs3500.hw02;

public enum Values {



  King(13, "K"),
  Queen(12, "Q"),
  Jack(11, "J"),
  Ten(10, "10"),
  Nine(9, "9"),
  Eight(8, "8"),
  Seven(7, "7"),
  Six(6, "6"),
  Five(5, "5"),
  Four(4, "4"),
  Three(3, "3"),
  Two(2, "2"),
  Ace(1, "A");












  private final int value;
  private final String id;

  Values(int value, String id) {
    this.value = value;
    this.id = id;
  }


  @Override
  public String toString() {
    return this.id;
  }

  protected int getValue() {
    return this.value;
  }




}
